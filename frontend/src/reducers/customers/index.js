import fetch from 'cross-fetch';

const GET_CUSTOMERS = 'GET_CUSTOMERS';
const GET_CUSTOMERS_SUCCESS = 'GET_CUSTOMERS_SUCCESS';
const GET_CUSTOMERS_FAILURE = 'GET_CUSTOMERS_FAILURE';

export const requestCustomers = () => ({ type: GET_CUSTOMERS });
export const receiveCustomers = customers => ({ type: GET_CUSTOMERS_SUCCESS, customers });
export const receiveCustomersFail = error => ({ type: GET_CUSTOMERS_FAILURE, error });

export const fetchCustomers = () => (dispatch) => {
  dispatch(requestCustomers());
  return fetch('/api/v1/customers/').then(
    res => res.json(),
    err => dispatch(receiveCustomersFail(err))
  )
    .then(customers => dispatch(receiveCustomers(customers)))
};

const initialState = {
  customers: [],
  isLoading: false,
  error: false
};

export default (state = initialState, action) => {
  switch(action.type) {
    case GET_CUSTOMERS:
      return {
        ...state,
        isLoading: true
      };
    case GET_CUSTOMERS_SUCCESS:
      return {
        ...state,
        customers: action.customers,
        isLoading: false
      };
    case GET_CUSTOMERS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false
      };
    default:
      return state;
  }
};
