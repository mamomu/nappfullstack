import fetch from 'cross-fetch';

const GET_PRODUCTS = 'GET_PRODUCTS';
const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';
const CHANGE_PRODUCT = 'CHANGE_PRODUCT';

export const requestProducts = () => ({ type: GET_PRODUCTS });
export const receiveProducts = products => ({ type: GET_PRODUCTS_SUCCESS, products });
export const receiveProductsFail = error => ({ type: GET_PRODUCTS_FAILURE, error });
export const changeProduct = (product) => ({ type: CHANGE_PRODUCT, product });
export const removeProduct = (product) => ({ type: CHANGE_PRODUCT, product: "" });

export const fetchProducts = () => (dispatch) => {
  dispatch(requestProducts());
  return fetch('/api/v1/products/').then(
    res => res.json(),
    err => dispatch(receiveProductsFail(err))
  )
    .then(products => dispatch(receiveProducts(products)))
};

export const setProduct = (product) => (dispatch) => {
  dispatch(changeProduct(product));
};

export const clearProduct = (product) => (dispatch) => {
  dispatch(removeProduct(product));
};

const initialState = {
  products: [],
  product: "",
  isLoading: false,
  error: false
};

export default (state = initialState, action) => {
  switch(action.type) {
    case GET_PRODUCTS:
      return {
        ...state,
        isLoading: true
      };
    case GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.products,
        isLoading: false
      };
    case GET_PRODUCTS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false
      };
    case CHANGE_PRODUCT:
      return {
        ...state,
        product: action.product
      };
    default:
      return state;
  }
};
