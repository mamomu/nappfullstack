import React from "react";
import "./quantity.scss";
import { connect } from "react-redux";
import * as products from "../../reducers/products";
import * as customers from "../../reducers/customers";
import { bindActionCreators } from "redux";
import Typography from "@material-ui/core/Typography";

class Quantity extends React.Component {
  changeProduct(item) {
    this.props.productsActions.setProduct({
      ...this.props.product,
      ...item
    });
  }

  decrement() {
    this.changeProduct({
      quantity:
        this.props.product.quantity > this.props.product.multiple
          ? (this.props.product.quantity || 0) - this.props.product.multiple
          : this.props.product.multiple
    });
  }

  increment() {
    this.changeProduct({
      quantity: (this.props.product.quantity || 0) + this.props.product.multiple
    });
  }

  render() {
    return (
      <div>
        <Typography gutterBottom variant="body2">
          Quantidade
        </Typography>
        <div className="quantity-input">
          <button
            className="quantity-input__modifier quantity-input__modifier--left"
            onClick={this.decrement.bind(this)}
          >
            &mdash;
          </button>
          <input
            className="quantity-input__screen"
            type="text"
            placeholder={this.props.product.multiple}
            value={this.props.product.quantity}
            readOnly
          />
          <button
            className="quantity-input__modifier quantity-input__modifier--right"
            onClick={this.increment.bind(this)}
          >
            &#xff0b;
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.productsReducer.products,
  product: state.productsReducer.product,
  customers: state.customersReducer.customers
});

const mapDispatchToProps = dispatch => ({
  productsActions: bindActionCreators(products, dispatch),
  customersActions: bindActionCreators(customers, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Quantity);
