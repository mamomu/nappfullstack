const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let OrderSchema = new Schema({
  items: { type: Array, required: true },
  customer: { type: Schema.ObjectId, required: true }
});

module.exports = mongoose.model("Order", OrderSchema);
