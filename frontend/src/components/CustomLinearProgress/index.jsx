import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";

import { getProfitability } from "../../helpers/profitability";

function CustomLinearProgress(props) {
  const { classes, product } = props;
  return (
    <div style={{ width: "100%", height: 5, background: "#efefef" }}>
      <span
        className="transition"
        style={{
          width: `${props.value}%`,
          display: "block",
          height: 5,
          background: props.color
        }}
      />
    </div>
  );
}

export default CustomLinearProgress;
