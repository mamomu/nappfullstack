# Napp Solutions FullStack

## Application (Backend / NodeJS)

### Installation

Use the package manager [npm] to install.

```bash
npm install
```

## Usage

```bash
node app.js [Port 3000]

http://localhost:3000
```
## API Documentation on: [Postman](https://documenter.getpostman.com/view/7369109/S1LzwmCj?version=latest)

## Application (Frontend / ReactJS)

### Installation

Use the package manager [npm] to install.

```bash
cd frontend
npm install

```

## Build Frontend

```bash
npm run build

```


