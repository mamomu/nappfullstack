import React, { Component } from 'react';
import './title.scss';

class Title extends Component {
  
  render() {
    const {children} = this.props;
    
    return (
        <div className="title" style={{color: this.props.color}}>
            {children}      
        </div>
    );
  }
}


export default Title;
