const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
    name: {type: String, required: true, max: 100},
    price: {type: Number, required: true, default: 0.00, min: 0.00},
    multiple: {type: Number, required: true, default: 0}
});

module.exports = mongoose.model('Product', ProductSchema);
