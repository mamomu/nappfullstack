export const getProfitability = (cost, gains) => {
  cost = parseFloat(cost);
  gains = parseFloat(gains);
  let top, bottom;

  top = gains - cost;
  bottom = top / cost;

  let value = bottom;

  value = value * 100;

  if (gains > cost) {
    return {
      label: "Ótima",
      color: "#087512",
      value: 100
    };
  } else if (gains <= cost && value >= -10) {
    return {
      label: "Boa",
      color: "#c5a90b",
      value: 50
    };
  } else {
    return {
      label: "Ruim",
      color: "#9a0827",
      value: 0
    };
  }
};
