import React from "react";

import "./App.scss";
import MainScreen from "./screens/main";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import indigo from "@material-ui/core/colors/indigo";
import white from "@material-ui/core/colors/grey";
import red from "@material-ui/core/colors/red";

const theme = createMuiTheme({
  palette: {
    primary: white,
    secondary: white,
    error: red,
    contrastThreshold: 3,
    tonalOffset: 1
  }
});

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <MainScreen />
    </MuiThemeProvider>
  );
};

export default App;
