import React, { Component } from "react";

import "./main.scss";

import { connect } from "react-redux";
import * as products from "../../reducers/products";
import * as customers from "../../reducers/customers";
import { bindActionCreators } from "redux";

import Title from "../../components/Title";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import CustomCard from "../../components/CustomCard";

import { getProfitability } from "../../helpers/profitability";
import { Paper } from "@material-ui/core";

import Snackbar from "@material-ui/core/Snackbar";
import Typography from "@material-ui/core/Typography";

import NumberFormat from "react-number-format";

class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: "",
      productAdded: false,
      customer: "",
      order: {
        customer: {},
        items: []
      }
    };
  }
  componentDidMount() {
    this.props.productsActions.fetchProducts();
    this.props.customersActions.fetchCustomers();
  }

  handleChange = event => {
    this.changeProduct({
      ...event.target.value,
      quantity: event.target.value.multiple,
      newPrice: event.target.value.price
    });
  };

  changeProduct(item) {
    this.props.productsActions.setProduct({
      ...this.props.product,
      ...item
    });
  }

  handleChangeCustomer = event => {
    this.setState({
      customer: event.target.value,
      order: { ...this.state.order, customer: event.target.value }
    });
  };

  handleChangeValue(a) {
    this.changeProduct({
      newPrice: a
    });
  }

  addProduct(product) {
    this.setState({
      productAdded: true,
      order: {
        ...this.state.order,
        items: [
          ...this.state.order.items,
          { price: product.newPrice, quantity: product.quantity, product }
        ]
      }
    });
    this.props.productsActions.clearProduct();
  }

  handleClose() {
    this.setState({
      productAdded: false
    });
  }

  render() {
    return (
      <div className="main" style={{ flexGrow: 1 }}>
        <Grid container spacing={24} className="grid">
          <Grid item sm={12} md={5} xs={12} className="container-left">
            <div className="padding-content">
              <Title color="#FFFFFF">Novo pedido</Title>
              <form className="form">
                <FormControl
                  fullWidth
                  variant="standard"
                  style={{ marginBottom: 30 }}
                >
                  <InputLabel style={{ color: "#ffffff" }}>Cliente</InputLabel>
                  <Select
                    value={this.state.customer}
                    onChange={this.handleChangeCustomer}
                    inputProps={{
                      classes: {
                        icon: "arrow"
                      }
                    }}
                    input={
                      <Input style={{ color: "#FFFFFF" }} name="customer" />
                    }
                  >
                    {this.props.customers.map((customer, key) => (
                      <MenuItem key={key} value={customer}>
                        {customer.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <FormControl
                  fullWidth
                  variant="standard"
                  style={{ marginBottom: 30 }}
                >
                  <InputLabel
                    htmlFor="product-input"
                    style={{ color: "#ffffff" }}
                  >
                    Selecione um produto
                  </InputLabel>
                  <Select
                    value={this.props.product}
                    onChange={this.handleChange}
                    inputProps={{
                      classes: {
                        icon: "arrow"
                      }
                    }}
                    renderValue={item => {
                      return item.name;
                    }}
                    input={
                      <Input
                        style={{ color: "#FFFFFF" }}
                        name="product"
                        id="product-input"
                      />
                    }
                  >
                    {this.props.products.map((product, key) => (
                      <MenuItem key={key} value={product}>
                        {product.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </form>

              {this.props.product && (
                <CustomCard
                  onChange={value => {
                    this.handleChangeValue(value);
                  }}
                  add={() => this.addProduct(this.props.product)}
                  product={this.props.product}
                />
              )}
            </div>
          </Grid>
          <Grid item sm={12} md={7} xs={12}>
            <div className="padding-content">
              <Title color="#666666">Resumo do pedido</Title>
              {/* <p></p> */}
              <Typography gutterBottom variant="h4" component="h4">
                {this.state.order.customer.name}
              </Typography>
              {this.state.order.items.length > 0 ? (
                this.state.order.items.map((i, index) => {
                  return (
                    <Paper style={{ padding: 20, marginTop: 20 }} key={index}>
                      <Grid container spacing={24}>
                        <Grid item xs={3} md={3} sm={3}>
                          <span style={{ fontSize: 14 }}>{i.product.name}</span>
                        </Grid>
                        <Grid item xs={2} md={2} sm={2}>
                          <span style={{ fontSize: 14 }}>
                            Qtd: <strong>{i.quantity}</strong>
                          </span>
                        </Grid>
                        <Grid item xs={4} md={4} sm={4}>
                          <span
                            style={{
                              fontSize: 14,
                              fontWeight: "bold",
                              color: getProfitability(i.product.price, i.price)
                                .color
                            }}
                          >
                            Rentabilidade{" "}
                            {getProfitability(i.product.price, i.price).label}
                          </span>
                        </Grid>
                        <Grid item xs={3} md={3} sm={3}>
                          <span
                            style={{
                              fontSize: 14,
                              fontWeight: "bold",
                              textAlign: "right"
                            }}
                          >
                            <NumberFormat
                              value={i.price}
                              displayType={"text"}
                              thousandSeparator={"."}
                              decimalSeparator={","}
                              fixedDecimalScale={true}
                              decimalScale={2}
                              prefix={"R$"}
                            />
                          </span>
                        </Grid>
                      </Grid>
                    </Paper>
                  );
                })
              ) : (
                <Paper style={{ padding: 20, marginTop: 20 }}>
                  <div>Nenhum item encontrado!</div>
                </Paper>
              )}
              <Snackbar
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left"
                }}
                open={this.state.productAdded}
                autoHideDuration={6000}
                onClose={this.handleClose.bind(this)}
                ContentProps={{
                  "aria-describedby": "message-id"
                }}
                message={
                  <span id="message-id">Produto adicionado com sucesso!</span>
                }
              />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.productsReducer.products,
  product: state.productsReducer.product,
  customers: state.customersReducer.customers
});

const mapDispatchToProps = dispatch => ({
  productsActions: bindActionCreators(products, dispatch),
  customersActions: bindActionCreators(customers, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen);
