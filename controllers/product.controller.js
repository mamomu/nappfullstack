const Product = require('../models/product.model');

exports.create = (req, res) => {
    let product = new Product(
        {   
            name: req.body.name,
            price: req.body.price,
            multiple: req.body.multiple
        }
    );

    product.save(err => {
        if (err) {
            return next(err);
        }
        res.status(201).json({message: "Produto criado com sucesso!", product});
    })
};

exports.update = (req, res) => {
    Product.updateOne({_id: req.params.id}, {$set: req.body}, (err) => {
        if (err) return next(err);
        
        Product.findById(req.params.id, (err, item) => {
            if (err) return next(err);
            res.status(202).json({message: "Produto atualizado com sucesso!", product: item});
        })
    });
};

exports.list = (req, res) => {
    Product.find((err, products) => {
        if (err) return next(err);
        res.json(products);
    });
};

exports.get = (req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (err) return next(err);
        res.json(product);
    })
};


exports.delete = (req, res) => {
    Product.deleteOne({_id: req.params.id}, (err) => {
        if (err) return next(err);
        res.json({message: "Produto removido com sucesso!"});
    })
};
