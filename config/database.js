const mongoose = require('mongoose');
let dbUrl = 'mongodb://murbach:e845ab2@157.230.178.119:27017/napp';

let mongoDB = process.env.MONGODB_URI || dbUrl;

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
