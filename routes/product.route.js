const express = require('express');
const router = express.Router();

const product = require('../controllers/product.controller');

router.post('/', product.create);
router.get('/', product.list);
router.delete('/:id', product.delete);
router.put('/:id', product.update);
router.get('/:id', product.get);

module.exports = router;
