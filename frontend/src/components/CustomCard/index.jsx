import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import { getProfitability } from "../../helpers/profitability";

import Quantity from "../Quantity";
import Grid from "@material-ui/core/Grid";
import CustomLinearProgress from "../CustomLinearProgress";
import NumberFormat from "react-number-format";

const styles = (props, test) => {
  return {
    card: {
      maxWidth: "100%"
    },
    media: {
      objectFit: "cover"
    }
  };
};

function CustomCard(props) {
  const { classes, product } = props;
  return (
    <Card className={classes.card}>
      <div>
        <CardContent>
          <Typography gutterBottom variant="title" component="h2">
            {product.name}
          </Typography>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={12} md={6}>
              <Typography gutterBottom variant="body2">
                Preço unitário original:{" "}
                <strong>
                  <NumberFormat
                    value={product.price}
                    displayType={"text"}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    fixedDecimalScale={true}
                    decimalScale={2}
                    prefix={"R$"}
                  />
                </strong>
              </Typography>
              <NumberFormat
                label="Preço unitário"
                name="newPrice"
                fullWidth
                style={{ marginTop: 30 }}
                onValueChange={value => {
                  props.onChange(value.floatValue);
                }}
                value={product.newPrice}
                placeholder="0,00"
                customInput={TextField}
                thousandSeparator={"."}
                decimalSeparator={","}
                fixedDecimalScale={true}
                decimalScale={2}
                prefix={"R$"}
              />

              <div style={{ marginTop: 30 }}>
                <Quantity />
              </div>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <Typography gutterBottom variant="body2">
                Rentabilidade
              </Typography>

              <span
                className="transition"
                style={{
                  color: getProfitability(product.price, product.newPrice)
                    .color,
                  fontSize: 30,
                  fontWeight: "bold"
                }}
              >
                {getProfitability(product.price, product.newPrice).label}
              </span>
              <CustomLinearProgress
                value={getProfitability(product.price, product.newPrice).value}
                color={getProfitability(product.price, product.newPrice).color}
              />
            </Grid>
          </Grid>
        </CardContent>
      </div>
      <CardActions style={{ textAlign: "right" }}>
        <Button
          fullWidth={true}
          onClick={props.add}
          disabled={
            getProfitability(product.price, product.newPrice).value === 0
          }
          size="small"
          color="secondary"
        >
          {getProfitability(product.price, product.newPrice).value === 0
            ? "Rentabilidade abaixo do esperado"
            : "Adicionar ao pedido"}
        </Button>
      </CardActions>
    </Card>
  );
}

CustomCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomCard);
