import { combineReducers } from 'redux';
import productsReducer from './products';
import customersReducer from './customers';

export default combineReducers({
    productsReducer,
    customersReducer
  });
  
