const Customer = require("../models/customer.model");

exports.create = (req, res) => {
  let customer = new Customer({
    name: req.body.name
  });

  customer.save(err => {
    if (err) {
      return next(err);
    }
    res.status(201).json({ message: "Cliente criado com sucesso!", customer });
  });
};

exports.update = (req, res) => {
  Customer.updateOne({ _id: req.params.id }, { $set: req.body }, err => {
    if (err) return next(err);

    Customer.findById(req.params.id, (err, item) => {
      if (err) return next(err);
      res
        .status(202)
        .json({ message: "Cliente atualizado com sucesso!", customer: item });
    });
  });
};

exports.list = (req, res) => {
  Customer.find((err, customers) => {
    if (err) return next(err);
    res.json(customers);
  });
};

exports.get = (req, res) => {
  Customer.findById(req.params.id, (err, customer) => {
    if (err) return next(err);
    res.json(customer);
  });
};

exports.delete = (req, res) => {
  Customer.deleteOne({ _id: req.params.id }, err => {
    if (err) return next(err);
    res.json({ message: "Cliente removido com sucesso!" });
  });
};
