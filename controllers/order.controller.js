const Order = require("../models/order.model");

exports.create = (req, res) => {
  let order = new Order({
    items: req.body.items
  });

  order.save(err => {
    if (err) {
      return next(err);
    }
    res.status(201).json({ message: "Cliente criado com sucesso!", order });
  });
};

exports.update = (req, res) => {
  Order.updateOne({ _id: req.params.id }, { $set: req.body }, err => {
    if (err) return next(err);

    Order.findById(req.params.id, (err, item) => {
      if (err) return next(err);
      res
        .status(202)
        .json({ message: "Cliente atualizado com sucesso!", order: item });
    });
  });
};

exports.list = (req, res) => {
  Order.find((err, orders) => {
    if (err) return next(err);
    res.json(orders);
  });
};

exports.get = (req, res) => {
  Order.findById(req.params.id, (err, order) => {
    if (err) return next(err);
    res.json(order);
  });
};

exports.delete = (req, res) => {
  Order.deleteOne({ _id: req.params.id }, err => {
    if (err) return next(err);
    res.json({ message: "Cliente removido com sucesso!" });
  });
};
