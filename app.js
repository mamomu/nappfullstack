const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const router = express.Router();
const path = require('path')

require("./config/database");

const product = require("./routes/product.route");
const customer = require("./routes/customer.route");
const order = require("./routes/order.route");

process.env.PWD = process.cwd();

app.use(express.static(path.join(process.env.PWD, 'public')));
app.get('/', function(req, res) {
  res.sendfile(path.join(__dirname, 'index.html'));
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.all("/*", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

router.use("/products", product);
router.use("/customers", customer);
router.use("/orders", order);

app.use("/api/v1/", router);

let port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log("Running server " + port);
});
