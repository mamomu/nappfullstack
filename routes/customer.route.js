const express = require("express");
const router = express.Router();

const customer = require("../controllers/customer.controller");

router.post("/", customer.create);
router.get("/", customer.list);
router.delete("/:id", customer.delete);
router.put("/:id", customer.update);
router.get("/:id", customer.get);

module.exports = router;
